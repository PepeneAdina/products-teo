#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "PerishableProduct.h"
#include "NonperishableProduct.h"

#pragma region Phase1

NonperishableProduct::Type toType(const std::string& dateOrType)
{
	if (dateOrType == "Clothing")
		return NonperishableProduct::Type::Clothing;
	if (dateOrType == "PersonalHygiene")
		return NonperishableProduct::Type::PersonalHygiene;
	if (dateOrType == "SmallAppliences")
		return NonperishableProduct::Type::SmallAppliences;
	throw "Invalid NonperishableProductType!";
	//aceasta este o exceptie care poate returna orice tip de data
}

std::vector<Product*> readProducts(const std::string& filename)
{
	//std::ifstream productFile(filename); //il sterg ca sa nu am doua fisiere
	std::vector<Product*> products;

	//while (!productFile.eof())
	for (std::ifstream productFile(filename); !productFile.eof();)
		//deschide fisierul doar la initializare
	{
		int32_t id;
		std::string name;
		float price;
		int32_t vat;
		std::string dateOrType;

		productFile >> id >> name >> price >> vat >> dateOrType;
		if (std::isdigit(dateOrType[0]))
			products.emplace_back(new PerishableProduct(id, name, price, dateOrType));
		else
		{
			//am modificat codul pentru a fi mai lizibil si am facut o functie mai sus
			//am facut try catch pentru ca avem exceptie in functie
			try
			{
				NonperishableProduct::Type type = toType(dateOrType);
				products.emplace_back(new NonperishableProduct(id, name, price, type));
			}
			catch (const char* mesage)
			{
				std::cout << mesage << '\n';
			}
		}
	}
	//productFile.close(); //nu mai trebuie, deoarece se inchide automat pentru ca este local
	return products;
}

void printProducts(const std::vector<Product*>& products)
{
	for (const auto& product : products)
		std::cout << product->getID() << " " << product->getName() << " " << product->getPrice() << "\n";
}

void printNonPerishableProducts(const std::vector<Product*>& products)
{
	for (const auto& product : products) //orice tip de data care are begin si end
	{
		if (dynamic_cast<NonperishableProduct*>(product)!=nullptr)
			std::cout << product->getPrice() << "\n";
	}
}

bool nameCompare(const Product* p1, const Product* p2) //am adaugat const
{
	return p1->getName() < p2->getName();
}

bool priceCompare(const Product* p1,const Product* p2) //am adaugat const
{
	return p1->getPrice() < p2->getPrice();
}

void sortByName(std::vector<Product*>& products)
{
	std::sort(products.begin(), products.end(), nameCompare);
}


void sortByPrice(std::vector<Product*>& products)
{
	std::sort(products.begin(), products.end(), priceCompare);
}

void printSortedProducts(std::vector<Product*>& products)
{
	std::cout << "How would you like your products to be sorted?\n"
		"1 - By Name\n  2 - By Price\n  Other - Unsorted\n";
	char option;
	std::cin >> option;
	switch (option)
	{
	case '1':
		sortByName(products);
		break;
	case '2':
		sortByPrice(products);
		break;
	default:
		break;
	}
	printProducts(products);
}
#pragma endregion Phase1

int main()
{
	std::vector<Product*> products = readProducts("Products.prodb");
	printNonPerishableProducts(products);
	printSortedProducts(products);
	return 0;
}