#pragma once
#include <string>
#include <cstdint>
#include "IPriceable.h"

class Product :public IPriceable
{
public:
	Product(int32_t id, const std::string& name, float rawPrice);

	uint16_t getID() const;
	const std::string& getName() const; //referinta nu face copie inutila la membrii
	float getRawPrice() const;
	//acest const inseamna ca in functie nu se schimba pointerul catre obiectul curent
	//primul parametru this este const

protected:
	int32_t m_id; //de preferabia uint16_t
	std::string m_name;
	float m_rawPrice;
};