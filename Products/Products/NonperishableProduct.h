#pragma once
#include "Product.h"

//enum class NonperishableProductType
//{
//	se pot face atribuiri, operatii aritmetice, shiftare la stanga
//	Clothing=1,
//	SmallAppliences=Clothing<<1, //2 la 1
//	PersonalHygiene= SmallAppliences<<1 //2 la 2
//};

class NonperishableProduct :public Product
{
public:
	enum class Type
	{
		//se pot face atribuiri, operatii aritmetice, shiftare la stanga
		Clothing = 1,
		SmallAppliences = Clothing << 1, //2 la 1
		PersonalHygiene = SmallAppliences << 1 //2 la 2
	};
public:
	NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type type);
	~NonperishableProduct() override = default;

	Type getType() const;
	float getPrice() const override final;
	int32_t getVAT() const override final;
	//daca marcat o fucntie ca fiind 'final' nu o mai putem suprascrie
	//'override' metoda este virtuala undeva mai sus, 'override' gasi probleme de scriere


private:
	Type m_type;
};