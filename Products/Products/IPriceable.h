#pragma once
#include <cstdint>

class IPriceable
{
public:
	virtual int32_t getVAT() const = 0;
	virtual float getPrice() const = 0;
	//intr-o clasa de baza avem nevoie de destructor virtual pentru a sterge obiectul de jos in sus
	virtual ~IPriceable() = default;
};